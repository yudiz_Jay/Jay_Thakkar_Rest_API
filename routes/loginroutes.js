/**
 * Module dependencies.
 */

var mysql = require('mysql');
var express = require('express');
var app = express();
var bcrypt = require('bcrypt');
var random = require("random-js")(); // uses the nativeMath engine
var nodemailer = require('nodemailer');


// Connection With Database
var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 8889,
  database: 'Exam'
});
connection.connect(function (err) {
  if (!err) {
    console.log("Database is connected ... nn");
  } else {
    console.log("Error connecting database ... nn");
  }
});



// Registration of User

exports.register = function (req, res) {
  // console.log("req",req.body);

  var today = new Date();
  bcrypt.hash(req.body.password, 5, function (err, bcryptedPassword) {

    var users = {
      "firstname": req.body.firstname,
      "lastname": req.body.lastname,
      "email": req.body.email,
      "password": bcryptedPassword,
      "created": today,
      "modified": today
    }

    req.checkBody('firstname', 'First name required').notEmpty();
    req.checkBody('lastname', 'Last name required').notEmpty();
    req.checkBody('password', 'Password required').notEmpty();
    req.checkBody('password', '6 to 20 characters required').len(6, 20);
    req.checkBody('email', 'Email required').notEmpty();
    req.checkBody('email', 'Invalid Email').isEmail();
    var errors = req.validationErrors();

    if (errors) {
      res.send({
        "code": 200,
        data: errors
      });
    }
    else{
      connection.query('INSERT INTO user SET ?', users, function (error, results, fields) {

      if (errors) {
        res.json({
          status: 412,
          data: errors
        });
      } else {
        console.log('The solution is: ', results);
        res.send({
          "code": 200,
          "success": "user registered sucessfully"
        });
      }
    });
    }
  });
}



// Login User

exports.login = function (req, res) {
  var email = req.body.email;
  var password = req.body.password;
  connection.query('SELECT * FROM user WHERE email = ?', [email], function (error, results, fields) {
    if (error) {
      // console.log("error ocurred",error);
      res.send({
        "code": 400,
        "failed": "error ocurred"
      })
    } else {
      if (results.length > 0) {
        bcrypt.compare(password, results[0].password, function (err, doesMatch) {
          if (doesMatch) {
            res.send({
              "code": 200,
              "success": "login successfull"
            });
          } else {
            res.send({
              "code": 204,
              "success": "Email and password does not match"
            });
          }
        });
      } else {
        res.send({
          "code": 204,
          "success": "Email does not exits"
        });
      }
    }
  });
}



/*
 * GET User listing.
 */
exports.user = function (req, res) {
  var data = {
    "error": 1,
    "users": ""
  };

  connection.query("SELECT * from user", function (err, rows, fields) {
    if (rows.length != 0) {
      data["error"] = 0;
      data["users"] = rows;
      res.json(data);
    } else {
      data["users"] = 'No users Found..';
      res.json(data);
    }
  });
};


// Create New User

exports.create_user = function (req, res) {

  var today = new Date();
  var users = {
    "firstname": req.body.firstname,
    "lastname": req.body.lastname,
    "email": req.body.email,
    "created": today,
    "modified": today
  }

  req.checkBody('firstname', 'First name required').notEmpty();
  req.checkBody('lastname', 'Last name required').notEmpty();
  req.checkBody('email', 'Email required').notEmpty();
  req.checkBody('email', 'Invalid Email').isEmail();
  var errors = req.validationErrors();

   if (errors) {
      res.send({
        "code": 200,
        data: errors
      });
    }
  else{
    connection.query('INSERT INTO user SET ?', users, function (error, results, fields) {
    if (errors) {
      res.json({
        status: 412,
        data: errors
      });
    } else {
      console.log('The solution is: ', results);
      res.send({
        "code": 200,
        "success": "user Added sucessfully"
      });
  }
    
  
  });
  };
};



// Get User By Id

exports.get_user = function (req, res) {

  var id = req.params.id;
  console.log(' id ' + id);
  var data = {
    "error": 1,
    "users": ""
  };
  connection.query('SELECT * FROM user WHERE uid = ? ', [id], function (err, rows, fields) {
    console.log(rows)
    if (rows.length != 0) {
      data["error"] = 0;
      data["users"] = rows;
      res.json(data);
    } else {
      data["users"] = 'No users Found..';
      res.json(data);
    }
  });
};



// Update user

exports.update_user = function (req, res) {
  var id = req.params.id;
  var firstname = req.body.firstname;
  var lastname = req.body.lastname;
  var email = req.body.email;
  var data = {
    "error": 1,
    "user": ""
  };

  if (id && firstname && lastname && email) {
    connection.query("UPDATE user SET firstname=?, lastname=?, email=? WHERE uid=?", [firstname, lastname, email, id], function (err, rows, fields) {
      if (err) {
        data["user"] = "Error Updating data";
      } else {
        data["error"] = 0;
        data["user"] = "Updated User Successfully";
      }
      res.json(data);
    });
  } else {
    data["user"] = "Please provide all required data (i.e : id, firstname, lastname, email)";
    res.json(data);
  }
};




// Delete User

exports.delete_user = function (req, res) {
  var id = req.params.id;
  console.log(' id ' + id);
  var data = {
    "error": 1,
    "users": ""
  };

  connection.query('DELETE FROM user WHERE uid = ? ', [id], function (err, rows, fields) {
    //console.log(rows)
    var numRows = rows.affectedRows;
    if (numRows > 0) {
      if (rows.length != 0) {
        data["error"] = 0;
        data["users"] = 'Deleted User Successfully';
        res.json(data);
      }
    } else {
      data["users"] = 'No users Found..';
      res.json(data);
    }
  });
};



// Change Status

exports.change_status = function (req, res) {

  var uid = req.body.uid;
  var status = req.body.status;
  var data = {
    "error": 1,
    "message": ""
  };
  if (uid && status) {
    connection.query("UPDATE user SET status=? WHERE uid=?", [status, uid], function (err, result, fields) {

      if (err) {
        data["message"] = "Error Updating Status";
      } else {
        var numRows = result.affectedRows;
        if (numRows > 0) {
          if (status == 0) {
            data["error"] = 0;
            data["message"] = "User inactive";
          } else {
            data["error"] = 0;
            data["message"] = "User active";
          }
        } else {
          data["message"] = "User Not found";
        }
      }
      res.json(data);
    });
  } else {
    data["message"] = "Please provide all required data (i.e : id , Status)";
    res.json(data);
  }
};



// Forgot password

exports.fpass = function (req, res) {
  var email = req.body.email;
  var data = {
    "error": 1,
    "message": ""
  };

  connection.query('SELECT * FROM user WHERE email = ? ', [email], function (err, rows, fields) {
    console.log(rows[0])
    if (rows.length != 0) {
      var value = random.integer(111111, 999999);
      var transporter = nodemailer.createTransport({
        host: 'smtp.googlemail.com',
        port: 465,
        secure: true, // secure:true for port 465, secure:false for port 587
        auth: {
          user: 'jaythakkar1994@gmail.com',
          pass: '9638509800'
        }
      });

      var mailOptions = {
        from: 'jay.thakkar@ping2world.com', // sender address
        to: email, // list of receivers
        subject: 'One time password', // Subject line
        html: 'Here is your one time password <b>' + value + '</b>' // html body
      };
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        } else {
          connection.query("UPDATE user SET OTP=? WHERE uid=?", [value, rows[0].uid], function (err, rows, fields) {
            data["message"] = "Sent One time password check Your mail";
            res.json(data)
          });

          console.log('Message %s sent: %s', info.messageId, info.response);
        }
      });

      console.log(value);

    } else {
      data["users"] = 'No users Found..';
      res.json(data);
    }
  });
};



// Send OTP After Compare OTP And enter New Password

exports.cotp = function (req, res) {

  var email = req.body.email;
  var OTP = req.body.OTP;
  var password = req.body.password;

  var data = {
    "error": 1,
    "message": ""
  };
  req.checkBody('email', 'Email required').notEmpty();
  req.checkBody('email', 'Invalid Email').isEmail();
  req.checkBody('email', 'Email required').notEmpty();
  req.checkBody('password', 'Password requires').notEmpty();

  var errors = req.validationErrors();

  bcrypt.hash(req.body.password, 5, function (err, bcryptedPassword) {

    var users = {
      "password": bcryptedPassword
    }

    connection.query('SELECT * FROM user WHERE email = ? and OTP =? ', [email, OTP], function (err, rows, fields) {
      console.log(rows[0])
      if (rows.length != 0) {

        connection.query("UPDATE user SET password=? WHERE uid=?", [users.password, rows[0].uid], function (err, rows, fields) {
          if (errors) {
            res.json({
              status: 412,
              data: errors
            });
          } else {
            data["message"] = "Password Changed Successfully";
            res.json(data);
          }

        });

      } else {
        data["message"] = 'Wrong OTP or Email';
        res.json(data);
      }
    });
  });
};



// Change Password

exports.changepassword = function (req, res) {
  var uid = req.body.uid;
  var oldpassword = req.body.oldpassword;
  var newpassword = req.body.newpassword;
  var data = {
    "error": 1,
    "message": ""
  };
  // req.checkBody('oldpassword', 'oldpassword required').notEmpty();
  // req.checkBody('newpassword', 'newpassword required').notEmpty();
  // var errors = req.validationErrors();

  bcrypt.hash(req.body.oldpassword, 5, function (err, bcryptedPassword) {

    var users = {
      "password": bcryptedPassword
    }

    connection.query('SELECT * FROM user WHERE uid = ? ', [uid], function (err, rows, fields) {
      console.log(rows[0])

      if (rows.length != 0) {
        bcrypt.compare(oldpassword, rows[0].password, function (err, doesMatch) {
          if (doesMatch) {
            bcrypt.hash(req.body.newpassword, 5, function (err, bcryptedPassword) {

              var pass = {
                "password": bcryptedPassword
              }
              connection.query("UPDATE user SET password=? WHERE uid=?", [pass.password, rows[0].uid], function (errors, rows, fields) {
                if (errors) {
                  res.json({
                    status: 412,
                    data: errors
                  });
                } else {
                  data["message"] = "Password Changed Successfully";
                  res.json(data);
                }
              });
            });
          } else {
            res.send({
              "code": 204,
              "success": "Password does not match"
            });
          }
        });
      }
    });
  });
};


// Social SignUp
exports.socialsignup = function (req, res) {

  var today = new Date();

  var users = {
    "firstname": req.body.firstname,
    "lastname": req.body.lastname,
    "email": req.body.email,
    "social_id": req.body.social_id,
    "social_provider": req.body.social_provider,
    "created": today,
    "modified": today
  }
  req.checkBody('firstname', 'First name required').notEmpty();
  req.checkBody('lastname', 'Last name required').notEmpty();
  req.checkBody('email', 'Email required').notEmpty();
  req.checkBody('email', 'Invalid Email').isEmail();
  req.checkBody('social_id', 'Social-Id requires').notEmpty();
  req.checkBody('social_provider', 'Social-Provider requires').notEmpty();
  var errors = req.validationErrors();

  if (errors) {
      res.send({
        "code": 200,
        data: errors
      });
    }
  else{
    connection.query('INSERT INTO user SET ?', users, function (error, results, fields) {
    if (errors) {
      res.json({
        status: 412,
        data: errors
      });
    } else {
      console.log('The solution is: ', results);
      res.send({
        "code": 200,
        "success": "user registered sucessfully"
      });
    }
  });
  }
};



// Social Login 

exports.sociallogin = function (req, res) {
  var email = req.body.email;
  var social_id = req.body.social_id;
  connection.query('SELECT * FROM user WHERE email = ? and social_id = ? ', [email, social_id], function (error, results, fields) {
    if (error) {
      // console.log("error ocurred",error);
      if (errors) {
        res.json({
          status: 412,
          data: errors
        });
      } else {
        if (results.length > 0) {
          res.send({
            "code": 200,
            "success": "login successfull"
          });
        } else {
          res.send({
            "code": 204,
            "success": "Email does not exits"
          });
        }
      }
    }
  });
}



// Site Setting
exports.site_setting = function (req, res) {

  var today = new Date();

  var users = {
    "oname": req.body.oname,
    "ovalue": req.body.ovalue,
  }

  req.checkBody('oname', ' Name required').notEmpty();
  req.checkBody('ovalue', 'Value required').notEmpty();
  var errors = req.validationErrors();

  connection.query('INSERT INTO user SET ?', users, function (error, results, fields) {

    if (errors) {
      res.json({
        status: 412,
        data: errors
      });
    } else {
      console.log('The solution is: ', results);
      res.send({
        "code": 200,
        "success": "setting stored successfully"
      });
    }
  });
};



// get site setting 

exports.get_site_setting = function (req, res) {
  var data = {
    "error": 1,
    "message": ""
  };

  var id = req.params.id;
  console.log(' id ' + id);
  var data = {
    "error": 1,
    "users": ""
  };


  connection.query('SELECT * FROM options WHERE oid = ? ', [id], function (err, rows, fields) {
    console.log(rows)
    if (rows.length != 0) {
      data["error"] = 0;
      data["users"] = rows;
      res.json(data);
    } else {
      data["users"] = 'No Setting Found..';
      res.json(data);
    }
  });
}