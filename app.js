/**
 * Module dependencies.
 */
var express    = require("express");
var login = require('./routes/loginroutes');
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt');
var expressValidator = require('express-validator');

//middleware
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressValidator());


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var router = express.Router();

// test route
router.get('/', function(req, res) {
    res.json({ message: 'Welcome to our Exam module API' });
});


//route to handle user registration
router.post('/register',login.register); //routes of registration
router.post('/login',login.login); //routes of login
router.get('/user',login.user); // routes of user
router.post('/create_user',login.create_user); // routes of user creation
router.get('/get_user/:id', login.get_user); // routes of get user by id
router.post('/update_user/:id',login.update_user); // routes of update user
router.get('/delete_user/:id', login.delete_user); // routes of delete user
router.post('/change_status',login.change_status); // routes of change status
router.post('/fpass',login.fpass); // routes of forgot password
router.post('/cotp',login.cotp); // routes of compare otp and new passsword
router.post('/changepassword',login.changepassword); // routes of change password 
router.post('/socialsignup',login.socialsignup); // routes of social signup
router.post('/sociallogin',login.sociallogin); // routes of social login
router.post('/site_setting',login.site_setting);// routes of site_setting
router.get('/get_site_setting/:id',login.get_site_setting)


app.use('/api', router);

// Port number servar started in this port
app.listen(5000);

module.exports = router;